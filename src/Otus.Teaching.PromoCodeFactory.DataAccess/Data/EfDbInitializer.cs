﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            var firstRun = !_dataContext.Database.GetService<IRelationalDatabaseCreator>().HasTables();

/*            try
            {
                var count = _dataContext.Roles.Count();
                firstRun = false;
            }
            catch (Exception)
            {
                // ignored
            }
*/
            _dataContext.Database.Migrate();

            if (!firstRun) return;

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            var partners = FakeDataFactory.Partners;

            foreach (var limit in partners.SelectMany(partner => partner.PartnerLimits))
            {
                limit.CreateDate = DateTime.SpecifyKind(limit.CreateDate, DateTimeKind.Utc);
                limit.EndDate = DateTime.SpecifyKind(limit.EndDate, DateTimeKind.Utc);
                if (limit.CancelDate != null)
                    limit.CancelDate = DateTime.SpecifyKind((DateTime)limit.CancelDate, DateTimeKind.Utc);
            }

            _dataContext.AddRange(partners);
            _dataContext.SaveChanges();
        }
    }
}