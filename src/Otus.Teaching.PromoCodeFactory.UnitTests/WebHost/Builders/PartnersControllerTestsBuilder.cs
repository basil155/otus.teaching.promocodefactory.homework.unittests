﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;

internal class PartnersControllerTestsBuilder
{
    private readonly IFixture _fixture = new Fixture().Customize( new AutoMoqCustomization());

    public T Create<T>()
    {
        return _fixture.Create<T>();
    }

    public Partner CreateActivePartner()
    {
        var partner = _fixture.Build<Partner>()
            .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
            .With(p => p.IsActive, true)
            .Create();
        var limits = CreateActivePartnerLimits(partner);
        partner.PartnerLimits = limits;

        return partner;
    }

    public ICollection<PartnerPromoCodeLimit> CreateActivePartnerLimits(Partner partner)
    {
        return _fixture.Build<PartnerPromoCodeLimit>()
            .With(l => l.CancelDate, null as DateTime?)
            .With(l => l.PartnerId, partner.Id)
            .With(l => l.Partner, partner)
            .CreateMany(1)
            .ToList();
    }
    
    public SetPartnerPromoCodeLimitRequest CreateCorrectRequest()
    {
        return _fixture.Build<SetPartnerPromoCodeLimitRequest>()
            .With(r => r.Limit, 1)
            .Create();
    }

    public DataContext CreateEfContext()
    {
        var options = new DbContextOptionsBuilder<DataContext>()
            .UseSqlite("Filename=TestDb.sqlite")
            //.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"))
            .UseSnakeCaseNamingConvention()
            .UseLazyLoadingProxies()
            .Options;

        return new DataContext(options);
    }

    public IRepository<Partner> CreateEfRepository(DataContext context)
    {
        return new EfRepository<Partner>(context);
    }

    public PartnersController CreateControllerWithDb(IRepository<Partner> repository)
    {
        return new PartnersController(repository);
    }
}