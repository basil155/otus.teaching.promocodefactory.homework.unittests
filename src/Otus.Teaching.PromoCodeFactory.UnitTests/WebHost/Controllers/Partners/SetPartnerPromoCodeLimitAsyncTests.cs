﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly PartnersControllerTestsBuilder _builder;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _builder = new PartnersControllerTestsBuilder();
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }


        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = _builder.Create<Guid>();

            Partner partner = null;

            var request = _builder.CreateCorrectRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
 
        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = _builder.CreateActivePartner();
            partner.IsActive = false;

            var request = _builder.CreateCorrectRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
 
        /// <summary>
        /// 3a. Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал
        /// NumberIssuedPromoCodes, если есть активный лимит
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsSetWithActive_NumberIssuedPromoCodesCleared()
        {
            // Arrange
            var partner = _builder.CreateActivePartner();
            partner.NumberIssuedPromoCodes = 1;

            var request = _builder.CreateCorrectRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }
 
        /// <summary>
        /// 3b. Если партнеру выставляется лимит, если прошлый лимит закончился, то количество не обнуляется
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsSetWithoutActive_NumberIssuedPromoCodesRemains()
        {
            var partner = _builder.CreateActivePartner();
            partner.NumberIssuedPromoCodes = 1;

            foreach (var l in partner.PartnerLimits)
            {
                l.CancelDate = DateTime.Now.AddMonths(-1);
                l.EndDate = DateTime.Now.AddMonths(-1);
            }

            var request = _builder.CreateCorrectRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(1);
        }
        
        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsSet_PreviousDeactivated()
        {
            // Arrange
            var partner = _builder.CreateActivePartner();

            var oldLimit = partner.PartnerLimits.Single(l => l.CancelDate == null);

            var request = _builder.CreateCorrectRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            oldLimit.CancelDate.Should().BeOnOrBefore(DateTime.Now);
        }

        /// <summary>
        /// 5a. Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IncorrectLimit_ReturnsBadRequest()
        {
            var partner = _builder.CreateActivePartner();

            var request = _builder.CreateCorrectRequest();
            request.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
                
        /// <summary>
        /// 5b. Неположительный лимит не должен приводить к изменениям
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_IncorrectLimit_DataNotChanged()
        {
            // Arrange
            var partner = _builder.CreateActivePartner();
            partner.NumberIssuedPromoCodes = 1;

            var oldLimit = partner.PartnerLimits.Single(l => l.CancelDate == null);

            var request = _builder.CreateCorrectRequest();
            request.Limit = 0;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(1);
            oldLimit.CancelDate.Should().BeNull();
        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsSet_LimitIsStored()
        {
            // Arrange
            var partner = _builder.CreateActivePartner();

            var request = _builder.CreateCorrectRequest();

            var context = _builder.CreateEfContext();
            await context.Database.EnsureDeletedAsync();
            await context.Database.EnsureCreatedAsync();

            var repository = _builder.CreateEfRepository(context);
            
            await context.AddAsync(partner);
            await context.SaveChangesAsync();

            var partnerController = _builder.CreateControllerWithDb(repository);

            // Act
            var result = await partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            var resultPartner = await repository.GetByIdAsync(partner.Id);
            var resultLimit = resultPartner.PartnerLimits.FirstOrDefault(l =>
                l.Limit == request.Limit
                && l.EndDate == request.EndDate);

            resultLimit.Should().NotBeNull();
        }
    }
}